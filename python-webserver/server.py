#!/usr/bin/python
from flask import Flask
from multiprocessing import Process

app = Flask(__name__)

port = 5000;
servers = [];

@app.route("/")
def index():
	return "This is a website served with python!"

@app.route("/kill")
def kill():
	if(len(servers) == 0):
		return "No more servers to kill"
	else:
		servers[len(servers)-1].terminate()
		servers.remove(servers[len(servers)-1])
		return "Killed server on port " + str((5000+len(servers)+1))	

@app.route("/add")
def add():
	addProcess()
	return "Added server on port " + str((5000+len(servers)))

@app.route("/mapping")
def mapping():
	return app.send_static_file('static.html')

def addProcess():
	server = Process(target=addServer)
	servers.append(server)
	server.start()

def addServer():
	global port
	port += len(servers)
	newapp = Flask(__name__)

	@newapp.route("/")
	def index():
		return "I am running on port: " + str(port)

	newapp.run(host='0.0.0.0', port=port)


if __name__ == '__main__':
	addProcess()
	app.run(host='0.0.0.0', port=port)
	