docker kill python_micro
docker rm python_micro

docker build -t python-webserver-microservice .

docker create -p 5000-5010:5000-5010 --name python_micro python-webserver-microservice

docker start python_micro