package de.mxdl.java_microservice;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static List<HttpServer> servers = new ArrayList<>();
    private static int port = 5000;

    public static void main(String[] args) throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new GetIndex());
        server.createContext("/add", new AddServer());
        server.createContext("/kill", new KillServer());
        server.createContext("/static", new StaticResponse());
        server.setExecutor(null);
        server.start();
        createServer();
    }

    private static void createServer()  throws IOException {
        port += 1;
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new GetPortIndex());
        server.setExecutor(null);
        servers.add(server);
        server.start();

    }

    static class GetIndex implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = "This is a website served with java!";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class AddServer implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {
            createServer();

            String response = "Server created on port " + port;
            t.sendResponseHeaders(200, response.length());
            OutputStream os = t.getResponseBody();
            os.write(response.getBytes());
            os.close();


        }
    }

    static class KillServer implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response;

            if (!(servers == null) && !(servers.size() == 0)) {
                HttpServer s = servers.get(servers.size() - 1);
                int killport = s.getAddress().getPort();
                s.stop(0);
                port -= 1;
                servers.remove(servers.size() - 1);


                response = "Killed server on port " + killport;
            } else {
                response = "No more servers to kill.";
            }
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class StaticResponse implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            Scanner in = new Scanner(new FileReader("/usr/src/java-webapp/static/static.html"));
            String response = in.toString();
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class GetPortIndex implements HttpHandler {
        @Override
        public void handle(HttpExchange httpExchange) throws IOException {
            String response = "I am running on port: " + httpExchange.getLocalAddress().getPort();
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }
}
